// Copyright Epic Games, Inc. All Rights Reserved.


#include "FW1GameModeBase.h"
#include "FW1_Carriage.h"
#include "FW1_GameInstance.h"
#include "FW1_PlayerController.h"
#include "FW1_PlayerState.h"
#include "Kismet/GameplayStatics.h"

AFW1GameModeBase::AFW1GameModeBase()
{
	DefaultPawnClass = AFW1_Carriage::StaticClass();
	PlayerControllerClass = AFW1_PlayerController::StaticClass();
	PlayerStateClass = AFW1_PlayerState::StaticClass();
}

void AFW1GameModeBase::StartPlay()
{
	Super::StartPlay();

	SetStatusGame(EFW1_StatusGame::inPlayGame);
}

// bool AFW1GameModeBase::SetPause(APlayerController* PC, FCanUnpause CanUnpauseDelegate)
// {
// 	// return Super::SetPause(PC, CanUnpauseDelegate);
// 	const auto PauseSet = Super::SetPause(PC, CanUnpauseDelegate);
// 	if(PauseSet)
// 	{
// 		SetStatusGame(EFW1_StatusGame::inPauseGame);
// 	}
//     
// 	return PauseSet;
// }

void AFW1GameModeBase::OnPauseGame()
{
	SetStatusGame(EFW1_StatusGame::inPauseGame);
	UGameplayStatics::SetGamePaused(GetWorld(), true);
}

bool AFW1GameModeBase::ClearPause()
{
	// return Super::ClearPause();
	const auto PauseCleared = Super::ClearPause();
	if (PauseCleared)
	{
		SetStatusGame(EFW1_StatusGame::inPlayGame);
	}
        
	return PauseCleared;
}

void AFW1GameModeBase::BeginPlay()
{
	Super::BeginPlay();

	// UGameplayStatics::PlaySound2D(GetWorld(), StartGameSound);
}

void AFW1GameModeBase::OnGameOver()
{
	UGameplayStatics::SetGamePaused(GetWorld(), true);
	SetStatusGame(EFW1_StatusGame::inGameOver);

	UFW1_GameInstance* myGameInst = Cast<UFW1_GameInstance>(GetGameInstance());
	if (myGameInst)
	{
		const auto myPlayerState = Cast<AFW1_PlayerState>(UGameplayStatics::GetPlayerState(GetWorld(),0));
		
		int32 CurrentScore = myPlayerState->GetScoresNum();

		// FString MSGtest = "score_" + FString::FromInt(CurrentScore);
		// GEngine->AddOnScreenDebugMessage(5, 2.0f, FColor::Red, MSGtest);
		
		// myGameInst->SetRecordScore(CurrentScore);
		myGameInst->SetNewRecordScore(CurrentScore);
	}
}

void AFW1GameModeBase::SetStatusGame(EFW1_StatusGame curStatusGame)
{
	if (curStatusGame == StatusGame) return;

	// FString DebMessage = "Changed status game: " + UEnum::GetValueAsString(curStatusGame);
	// // GEngine->AddOnScreenDebugMessage(5, 2.0f, FColor::Red, "Changed status game");
	// GEngine->AddOnScreenDebugMessage(5, 2.0f, FColor::Red, DebMessage);
	
	StatusGame = curStatusGame;
	OnChangedStatusGame.Broadcast(curStatusGame);
}
