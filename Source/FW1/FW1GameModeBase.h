// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "FW1_CoreTypes.h"
#include "GameFramework/GameModeBase.h"
#include "FW1GameModeBase.generated.h"


UCLASS()
class FW1_API AFW1GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	AFW1GameModeBase();

	FOnChangedStatusGame OnChangedStatusGame;

	virtual void StartPlay() override;

	// virtual bool SetPause(APlayerController* PC, FCanUnpause CanUnpauseDelegate = FCanUnpause()) override;
	void OnPauseGame();
	virtual bool ClearPause() override;

	void OnGameOver();
	
	// UPROPERTY(EditAnywhere)
	// int32 LivesGame = 3;

	virtual void BeginPlay() override;

private:
	EFW1_StatusGame StatusGame = EFW1_StatusGame::inPauseGame;
	
	void SetStatusGame(EFW1_StatusGame curStatusGame);
};
