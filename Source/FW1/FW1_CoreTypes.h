#pragma once

// #include "FW1_Ball.h"
#include "Engine/DataTable.h"
#include "FW1_CoreTypes.generated.h"

class USoundCue;

// !!! new features:
// a fortress wall, like a line.
// And the defense of the fortress wall.

UENUM(BlueprintType)
enum class EFW1_BonusTypes : uint8
{
	WithoutBonus = 0,
	IncreaseSpeed,
	DecreaseSpeed,
	AddBall,
	ExpansionCarriage,
	ContractionCarriage,
	ResetAllBonuses
};

UENUM(BlueprintType)
enum class EFW1_EventsForSoundVFX : uint8
{
	//for game and UI
	NONE = 0,
	MusicGame,
	// MusicMainMenu,
	PauseUI,
	SelectUI,
	BackUI,
	GameOver_win,
	GameOver_loss,

	//for ball
	Ball_Begin,
	Ball_Carriage,
	Ball_KillZ,

	//for block
	Block_BeginMove,
	Block_Carriage,
	Block_Damage,
	Block_Destroy,
	Block_KillZ
};

UENUM(BlueprintType)
enum class EFW1_StatusGame : uint8
{
	inMainMenu = 0,
	inPlayGame,
	inPauseGame,
	inGameOver
	// inStatistic
};

DECLARE_MULTICAST_DELEGATE_OneParam(FOnChangedStatusGame, EFW1_StatusGame);

USTRUCT(BlueprintType)
struct FSoundSetting : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		//TSubclassOf<EFW1_SoundVFX> SoundVFX = nullptr;
		EFW1_EventsForSoundVFX SoundVFX = EFW1_EventsForSoundVFX::Ball_Begin;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundCue* cueSound = nullptr;
	
};

USTRUCT(BlueprintType)
struct FProbabilityBlock : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<class AFW1_Block> TypeBlock = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 ProbabilityBlock = 0;
		
};

// // only for bonus balls
// USTRUCT()
// struct FArrayBalls
// {
// 	GENERATED_BODY()
//
// 	UPROPERTY()
// 	AFW1_Ball* PlayingBall;
//
// 	// UPROPERTY()
// 	// bool isBonus = false;
// };

// UCLASS()
// class FW1_API UFW1_CoreTypes : public UBlueprintFunctionLibrary
// {
// 	GENERATED_BODY()
//
// public:
// 	UFUNCTION(BlueprintCallable)
// 	static void PlaySoundForGame(EFW1_EventsForSoundVFX EventForSound);
// };

class FW1_CoreTypes
{
// public:
// 	static void PlaySoundForGame(EFW1_EventsForSoundVFX EventForSound);
};