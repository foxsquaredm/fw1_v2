// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/FW1_SoundComponent.h"
#include "FW1_GameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

// Sets default values for this component's properties
UFW1_SoundComponent::UFW1_SoundComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UFW1_SoundComponent::BeginPlay()
{
	Super::BeginPlay();

	// AActor* ComponentOwner = GetOwner();
	// if (ComponentOwner)
	// {
	// 	// ComponentOwner->OnPlaySoundEvents.AddDynamic(this, &UFW1_SoundComponent::PlayGameSoundEvent);
	// }
	
}

void UFW1_SoundComponent::PlayGameSoundEvent(EFW1_EventsForSoundVFX SoundVFX)
{
	if (SoundVFX == EFW1_EventsForSoundVFX::NONE) return;
	
	UFW1_GameInstance* myGameInst = Cast<UFW1_GameInstance>(GetWorld()->GetGameInstance());
	FSoundSetting curSoundSetting;
	if (myGameInst)
	{
		if (myGameInst->GetSoundSettingByName(SoundVFX, curSoundSetting))
		{
			UGameplayStatics::PlaySound2D(GetWorld(), curSoundSetting.cueSound);
		}
	}
}


// // Called every frame
// void UFW1_SoundComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
// {
// 	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
//
// 	// ...
// }

