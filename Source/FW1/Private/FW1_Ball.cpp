// Fill out your copyright notice in the Description page of Project Settings.


#include "FW1_Ball.h"
#include "FW1_GameInstance.h"
#include "FW1_PlayerState.h"
#include "GameFramework/PlayerState.h"
#include "GameFramework/KillZVolume.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

DEFINE_LOG_CATEGORY_STATIC(Log_testBall, All, All);

// Sets default values
AFW1_Ball::AFW1_Ball()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ConstructorHelpers::FObjectFinder<UStaticMesh> SphereMesh(TEXT("StaticMesh'/Engine/BasicShapes/Sphere.Sphere'"));
	
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("StaticMeshComponent");
	if(SphereMesh.Succeeded())
	{
		StaticMeshComponent->SetStaticMesh(SphereMesh.Object);
	}
	SetRootComponent(StaticMeshComponent);

	StaticMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	StaticMeshComponent->SetCollisionObjectType(ECollisionChannel::ECC_WorldStatic);
	// StaticMeshComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
	// StaticMeshComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);

	BallSpeed = GetDefaultSpeedBall();

	SoundComponent = CreateDefaultSubobject<UFW1_SoundComponent>(TEXT("SoundComponent"));
}

// Called when the game starts or when spawned
void AFW1_Ball::BeginPlay()
{
	Super::BeginPlay();

	StaticMeshComponent->OnComponentBeginOverlap.AddDynamic(this, &AFW1_Ball::OnBeginOverlap);
	StaticMeshComponent->SetGenerateOverlapEvents(true);

}

void AFW1_Ball::PostInitProperties()
{
	Super::PostInitProperties();
	
	// ChangeColor();

}

// Called every frame
void AFW1_Ball::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector curDeltaDirection = (BallSpeed * BallDirection) * DeltaTime;
	FHitResult OutSweepHitResult;
	AddActorWorldOffset(curDeltaDirection, true, &OutSweepHitResult);

	if (OutSweepHitResult.bBlockingHit)
	{
		FVector curNormal = OutSweepHitResult.Normal;
		BallDirection = FMath::GetReflectionVector(BallDirection, curNormal);
		// PlaySoundForGame(EFW1_EventsForSoundVFX::Ball_Carriage);
		SoundComponent->PlayGameSoundEvent(EFW1_EventsForSoundVFX::Ball_Carriage);
	}
}

void AFW1_Ball::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, 
                               AActor* OtherActor, UPrimitiveComponent* OtherComp, 
                               int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// UE_LOG(Log_testBall, Display, TEXT("event ball - OnBeginOverlap"));
	// GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10.f, FColor::Red, *FString::Printf(TEXT("Ball - Overlap Begin!")));
	
	const auto KZone = Cast<AKillZVolume>(OtherActor);
	if (KZone)
	{
		// PlaySoundForGame(EFW1_EventsForSoundVFX::Ball_KillZ);
		SoundComponent->PlayGameSoundEvent(EFW1_EventsForSoundVFX::Ball_KillZ);
		OnDeadBall.Broadcast(isBonus);		
		Destroy();
	}
}

void AFW1_Ball::ChangeSpeed(float Coefficient)
{
	if (Coefficient == 1)
	{
		BallSpeed = GetDefaultSpeedBall();
	}
	else
	{
		// float curSpeed = BallSpeed;
		// if ((Coefficient > 1 && curSpeed < 200) || (Coefficient < 1 && curSpeed > 10))
		if ((Coefficient > 1 && BallSpeed < 200) || (Coefficient < 1 && BallSpeed > 10))
		{
			BallSpeed = BallSpeed * Coefficient;
		}
	}
}

// void AFW1_Ball::PlaySoundForGame(EFW1_EventsForSoundVFX SoundVFX)
// {
// 	if (SoundVFX == EFW1_EventsForSoundVFX::NONE) return;
// 	
// 	UFW1_GameInstance* myGameInst = Cast<UFW1_GameInstance>(GetGameInstance());
// 	FSoundSetting curSoundSetting;
// 	if (myGameInst)
// 	{
// 		if (myGameInst->GetSoundSettingByName(SoundVFX, curSoundSetting))
// 		{
// 			UGameplayStatics::PlaySound2D(GetWorld(), curSoundSetting.cueSound);
// 		}
// 	}
// }

void AFW1_Ball::ChangeColor()
// void AFW1_Ball::ChangeColor(FLinearColor Color)
{
	// FLinearColor Color = FLinearColor(0.65f,0.2f,0,1);
	// const auto MaterialInst = StaticMeshComponent->CreateAndSetMaterialInstanceDynamic(0);
	// if (!MaterialInst) return;
	//
	// MaterialInst->SetVectorParameterValue("MaterialColorName", );

	UMaterialInterface* MI_Orange = LoadObject<UMaterialInterface>(nullptr, TEXT("/Game/test/MatInst_Orange.MatInst_Orange"));
	if (MI_Orange)
	{
		UE_LOG(Log_testBall, Display, TEXT("event ball - PostIP MI_Orange"));
		StaticMeshComponent->SetMaterial(0,MI_Orange);
		StaticMeshComponent->SetVectorParameterValueOnMaterials("ParamColor", FVector(0,0,0));
	}
}

float AFW1_Ball::GetDefaultSpeedBall()
{
	return 50.f;
}

