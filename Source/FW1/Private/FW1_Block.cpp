// Fill out your copyright notice in the Description page of Project Settings.


#include "FW1_Block.h"
#include "FW1_Ball.h"
#include "FW1_Carriage.h"
#include "FW1_GameInstance.h"
#include "GameFramework/KillZVolume.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

// Sets default values
AFW1_Block::AFW1_Block()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ConstructorHelpers::FObjectFinder<UStaticMesh> CubeMesh(TEXT("StaticMesh'/Engine/BasicShapes/Cube.Cube'"));
	MeshBox = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	if(CubeMesh.Succeeded())
	{
		MeshBox->SetStaticMesh(CubeMesh.Object);
		// const FVector ScaleCube = FVector(1.f, 1.f, 1.f);
		// MeshBox->SetRelativeScale3D(ScaleCube);
	}
	MeshBox->SetupAttachment(GetRootComponent());

	MeshBox->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshBox->SetCollisionObjectType(ECollisionChannel::ECC_WorldStatic);
	
	// MeshBox->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
	// MeshBox->BodyInstance.SetCollisionProfileName("BlockAllDynamic");

	SoundComponent = CreateDefaultSubobject<UFW1_SoundComponent>(TEXT("SoundComponent"));
}

// Called when the game starts or when spawned
void AFW1_Block::BeginPlay()
{
	Super::BeginPlay();

	MeshBox->OnComponentBeginOverlap.AddDynamic(this, &AFW1_Block::OnBeginOverlap);
	MeshBox->OnComponentHit.AddDynamic(this, &AFW1_Block::OnHit);
	MeshBox->SetGenerateOverlapEvents(true);
}

// Called every frame
void AFW1_Block::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (InMoving)
	{
		FVector curDeltaDirection = FVector(-8, 0,0);
		AddActorWorldOffset(curDeltaDirection, true);
	}
}

void AFW1_Block::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	const auto KZone = Cast<AKillZVolume>(OtherActor);
	if (KZone)
	{
		// PlaySoundForGame(EFW1_EventsForSoundVFX::Block_KillZ);
		// Destroy();
		OnDeadCurrentBlock(EFW1_EventsForSoundVFX::Block_KillZ);
	}
}

void AFW1_Block::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	FVector NormalImpulse, const FHitResult& Hit)
{
	// auto ValueOA = *FString::Printf(OtherActor);
	// // GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10.f, FColor::Orange, *FString::Printf(TEXT("Block - on hit!")));
	// GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10.f, FColor::Orange, ValueOA);

	if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL))
	{
		const auto ourBall = Cast<AFW1_Ball>(OtherActor);
		if (ourBall)
		{
			if (AmountDamageBeforMoving > 0)
			{
				AmountDamageBeforMoving--;
				// setmaterial -- darkening the color by 10-20 units
				// PlaySoundForGame(EFW1_EventsForSoundVFX::Block_Damage);
				SoundComponent->PlayGameSoundEvent(EFW1_EventsForSoundVFX::Block_Damage);
			}
			else if (AmountDamageBeforMoving == 0)
			{
				InMoving = true;
				// PlaySoundForGame(EFW1_EventsForSoundVFX::Block_BeginMove);
				SoundComponent->PlayGameSoundEvent(EFW1_EventsForSoundVFX::Block_BeginMove);
			}

			if (AmountDamage > 0)
			{
				// PlaySoundForGame(EFW1_EventsForSoundVFX::Block_Damage);
				SoundComponent->PlayGameSoundEvent(EFW1_EventsForSoundVFX::Block_Damage);
				AmountDamage--;
				// setmaterial -- darkening the color by 10-20 units
			}
			else if (AmountDamage == 0)
			{
				// PlaySoundForGame(EFW1_EventsForSoundVFX::Block_Destroy);
				// Destroy();
				OnDeadCurrentBlock(EFW1_EventsForSoundVFX::Block_Destroy);
			}
		}
		
		const auto ourCarriage = Cast<AFW1_Carriage>(OtherActor);
		if (ourCarriage)
		{
			// Destroy();
			OnDeadCurrentBlock();
		}
	}
}

// void AFW1_Block::PlaySoundForGame(EFW1_EventsForSoundVFX SoundVFX)
// {
// 	if (SoundVFX == EFW1_EventsForSoundVFX::NONE) return;
//
// 	UFW1_GameInstance* myGameInst = Cast<UFW1_GameInstance>(GetGameInstance());
// 	FSoundSetting curSoundSetting;
// 	if (myGameInst)
// 	{
// 		if (myGameInst->GetSoundSettingByName(SoundVFX, curSoundSetting))
// 		{
// 			UGameplayStatics::PlaySound2D(GetWorld(), curSoundSetting.cueSound);
// 		}
// 	}
// }

void AFW1_Block::ChangeColor()
// void AFW1_Ball::ChangeColor(FLinearColor Color)
{
	// reserve for programmatic color dimming, on damage
	
	// FLinearColor Color = FLinearColor(0.65f,0.2f,0,1);
	// const auto MaterialInst = MeshBox->CreateAndSetMaterialInstanceDynamic(0);
	// if (!MaterialInst) return;
	//
	// MaterialInst->SetVectorParameterValue("MaterialColorName", Color);

	//default color
	UMaterialInterface* MI_Orange = LoadObject<UMaterialInterface>(nullptr, TEXT("/Game/test/MatInst_Orange.MatInst_Orange"));
	if (MI_Orange)
	{
		// UE_LOG(Log_testBall, Display, TEXT("event ball - PostIP MI_Orange"));
		MeshBox->SetMaterial(0,MI_Orange);
		MeshBox->SetVectorParameterValueOnMaterials("ParamColor", FVector(0,0,0));
	}
}

void AFW1_Block::OnDeadCurrentBlock(EFW1_EventsForSoundVFX SoundVFX)
{
	InMoving = false;
	// PlaySoundForGame(SoundVFX);
	SoundComponent->PlayGameSoundEvent(SoundVFX);
	OnDeadBlock.Broadcast();
	Destroy();
}
