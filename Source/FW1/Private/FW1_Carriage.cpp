// Fill out your copyright notice in the Description page of Project Settings.


#include "FW1/Public/FW1_Carriage.h"
#include "FW1_Ball.h"
#include "FW1_Block.h"
#include "FW1_GameInstance.h"
#include "FW1_PlayerState.h"
#include "Components/BoxComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/PlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

// Sets default values
AFW1_Carriage::AFW1_Carriage()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ConstructorHelpers::FObjectFinder<UStaticMesh> CubeMesh(TEXT("StaticMesh'/Engine/BasicShapes/Cube.Cube'"));

	MeshCarriega = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	if (CubeMesh.Succeeded())
	{
		MeshCarriega->SetStaticMesh(CubeMesh.Object);
		const FVector ScaleCarriage = GetDefaultScaleCarriage();
		MeshCarriega->SetRelativeScale3D(ScaleCarriage);
		//MeshCarriega->SetMaterial()
	}
	MeshCarriega->SetupAttachment(GetRootComponent());

	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	// BoxCollision->SetBoxExtent(FVector(0.2f, 5.f, 1.f));
	const FVector ScaleBoxCollision = FVector(1.58f, 1.58f, 1.58f);
	BoxCollision->SetRelativeScale3D(ScaleBoxCollision);
	BoxCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	// BoxCollision->bDynamicObstacle = true;
	BoxCollision->SetupAttachment(MeshCarriega);

	BoxCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	BoxCollision->SetCollisionObjectType(ECollisionChannel::ECC_WorldStatic);

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	SphereCollision->InitSphereRadius(200.0f);
	const FVector LocSphereCollision = FVector(260.f, 0.f, 0.f);
	SphereCollision->SetRelativeLocation(LocSphereCollision);
	SphereCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	// SphereCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	// SetRootComponent(SphereCollision);
	SphereCollision->SetupAttachment(MeshCarriega);

	MovementComponent = CreateDefaultSubobject<UFloatingPawnMovement>(TEXT("MovementComponent"));
	SoundComponent = CreateDefaultSubobject<UFW1_SoundComponent>(TEXT("SoundComponent"));
}

// Called when the game starts or when spawned
void AFW1_Carriage::BeginPlay()
{
	Super::BeginPlay();

	BoxCollision->OnComponentBeginOverlap.AddDynamic(this, &AFW1_Carriage::OnBeginOverlap);
	BoxCollision->SetGenerateOverlapEvents(true);

	SpawnBall(false);
}

// // Called every frame
// void AFW1_Carriage::Tick(float DeltaTime)
// {
// 	Super::Tick(DeltaTime);
// }

// Called to bind functionality to input
void AFW1_Carriage::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	if (PlayerInputComponent)
	{
		PlayerInputComponent->BindAxis("MoveRight", this, &AFW1_Carriage::MoveRight);
	}
}

void AFW1_Carriage::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                   UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
                                   const FHitResult& SweepResult)
{
	// GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10.f, FColor::Orange, *FString::Printf(TEXT("Carriage - begin overlap!")));

	const auto onBlock = Cast<AFW1_Block>(OtherActor);
	if (onBlock)
	{
		const auto myPlayerState = Cast<AFW1_PlayerState>(GetPlayerState());
		myPlayerState->AddScore(onBlock->ScoreInBlock);

		// FString TextInfoScore = FString::Printf(TEXT("Score: %s "), *FString::FromInt(myPlayerState->GetScoresNum()));
		// GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10.f, FColor::Orange, *TextInfoScore);
		
		SoundComponent->PlayGameSoundEvent(EFW1_EventsForSoundVFX::Block_Carriage);
		
		auto BlockBonusType = onBlock->BonusType;
		switch (BlockBonusType)
		{
		case (EFW1_BonusTypes::IncreaseSpeed):
			{
				PlayingBall->ChangeSpeed(1.2);
				break;
			}
		case (EFW1_BonusTypes::DecreaseSpeed):
			{
				PlayingBall->ChangeSpeed(0.8);
				break;
			}
		case (EFW1_BonusTypes::AddBall):
			{
				SpawnBall(true);
				break;
			}
		case (EFW1_BonusTypes::ExpansionCarriage):
			{
				ChangeLongCarriage(1.2);
				break;
			}
		case (EFW1_BonusTypes::ContractionCarriage):
			{
				ChangeLongCarriage(0.8);
				break;
			}
		case (EFW1_BonusTypes::ResetAllBonuses):
			{
				ResetAllSettings();
				break;
			}
		default:
			break;
		}
	}
}

void AFW1_Carriage::MoveRight(float Amount)
{
	AddMovementInput(GetActorRightVector(), Amount);
}

// void AFW1_Carriage::PlaySoundForGame(EFW1_EventsForSoundVFX SoundVFX)
// {
// 	if (SoundVFX == EFW1_EventsForSoundVFX::NONE) return;
// 	
// 	UFW1_GameInstance* myGameInst = Cast<UFW1_GameInstance>(GetGameInstance());
// 	FSoundSetting curSoundSetting;
// 	if (myGameInst)
// 	{
// 		if (myGameInst->GetSoundSettingByName(SoundVFX, curSoundSetting))
// 		{
// 			UGameplayStatics::PlaySound2D(GetWorld(), curSoundSetting.cueSound);
// 		}
// 	}
// }

void AFW1_Carriage::SpawnBall(bool isBonus)
{
	UWorld* const World = GetWorld();
	if (!World) return;

	// const FTransform& PlaceBall = SphereCollision->GetComponentTransform();

	// ConstructorHelpers::FObjectFinder<UBlueprint> PutNameHere(TEXT("Blueprint'/Content/test/BP_test21.BP_test21'"));
	//
	//  if (PutNameHere.Object)
	//  {
	// 	BlueprintVar = (UClass*)PutNameHere.Object->GeneratedClass;
	//  }
	// AFW1_Ball* PlayingBall = GetWorld()->SpawnActor<AFW1_Ball>(BlueprintVar, PlaceBall);

	// FVector SpawnLocation = FVector(0);
	FVector SpawnLocation = this->GetActorLocation() + this->GetActorForwardVector() * 100;
	FRotator SpawnRotation = FRotator(0);

	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParams.Owner = this;
	SpawnParams.Instigator = GetInstigator();

	PlayingBall = Cast<AFW1_Ball>(GetWorld()->SpawnActor(DefaultBall, &SpawnLocation, &SpawnRotation, SpawnParams));
	// PlayingBall->ChangeColor();
	PlayingBall->isBonus = isBonus;

	PlayingBall->OnDeadBall.AddDynamic(this, &AFW1_Carriage::OnDeadPlayingBall);

	SoundComponent->PlayGameSoundEvent(EFW1_EventsForSoundVFX::Ball_Begin);
}

void AFW1_Carriage::OnDeadPlayingBall(bool isBonus)
{
	// GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10.f, FColor::Orange, *FString::Printf(TEXT("Carriage - ball dead!")));
	if (!isBonus)
	{
		const auto myPlayerState = Cast<AFW1_PlayerState>(GetPlayerState());
		myPlayerState->SubtractionLive();
		
		// FString TextInfoScore = FString::Printf(TEXT("Lives: %s "), *FString::FromInt(myPlayerState->GetLivesNum()));
		// GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10.f, FColor::Orange, *TextInfoScore);

		if (myPlayerState->GetLivesNum() >= 0)
		{
			SpawnBall(false);
			ResetAllSettings();
		}
	}
}

void AFW1_Carriage::ChangeLongCarriage(float Coefficient)
{
	if (Coefficient == 1)
	{
		FVector NewScale = GetDefaultScaleCarriage();
		MeshCarriega->SetRelativeScale3D(NewScale);
	}
	else
	{
		FVector curScale = MeshCarriega->GetRelativeScale3D();
		if ((Coefficient > 1 && curScale.Y < 15) || (Coefficient < 1 && curScale.Y > 5))
		{
			// 	double NewScaleY = curScale.Y + (Increase ? AmountChange : - AmountChange);
			double NewScaleY = curScale.Y * Coefficient;
			FVector NewScale = FVector(curScale.X, NewScaleY, curScale.Z);
			MeshCarriega->SetRelativeScale3D(NewScale);
		}
	}
}

FVector AFW1_Carriage::GetDefaultScaleCarriage()
{
	return FVector(0.2f, 5.f, 2.7f);
}

void AFW1_Carriage::ResetAllSettings()
{
	ChangeLongCarriage(1);
	PlayingBall->ChangeSpeed(1);
}
