// Fill out your copyright notice in the Description page of Project Settings.


#include "FW1_GameHUD.h"
#include "FW1/FW1GameModeBase.h"

DEFINE_LOG_CATEGORY_STATIC(LogFW1GameHUD, All, All);

void AFW1_GameHUD::DrawHUD()
{
	Super::DrawHUD();
}

void AFW1_GameHUD::BeginPlay()
{
	Super::BeginPlay();

	GameWidgets.Add(EFW1_StatusGame::inPlayGame, CreateWidget<UUserWidget>(GetWorld(), GameWidgetClass));
	GameWidgets.Add(EFW1_StatusGame::inPauseGame, CreateWidget<UUserWidget>(GetWorld(), PauseWidgetClass));
	GameWidgets.Add(EFW1_StatusGame::inGameOver, CreateWidget<UUserWidget>(GetWorld(), GameOverWidgetClass));
	
	for (auto GameWidgetPair : GameWidgets)
	{
		const auto GameWidget = GameWidgetPair.Value;
		if(!GameWidget) continue;
	
		GameWidget->AddToViewport();
		GameWidget->SetVisibility(ESlateVisibility::Hidden);
	}
    
	if(GetWorld())
	{
		const auto GameMode = Cast<AFW1GameModeBase>(GetWorld()->GetAuthGameMode());
		if (GameMode)
		{
			GameMode->OnChangedStatusGame.AddUObject(this, &AFW1_GameHUD::OnChangedStatusGame);
		}
	}
}

void AFW1_GameHUD::OnChangedStatusGame(EFW1_StatusGame StatusGame)
{
	if(CurrentWidget)
	{
		CurrentWidget->SetVisibility(ESlateVisibility::Hidden);    
	}
	
	if (GameWidgets.Contains(StatusGame))
	{
		CurrentWidget = GameWidgets[StatusGame];
	}
	
	if(CurrentWidget)
	{
		CurrentWidget->SetVisibility(ESlateVisibility::Visible); 
	}

	UE_LOG(LogFW1GameHUD, Display, TEXT("Game status changed: %s"), *UEnum::GetValueAsString(StatusGame));
}
