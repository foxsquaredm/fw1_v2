// Fill out your copyright notice in the Description page of Project Settings.


#include "FW1_GameInstance.h"
#include "FW1_SaveGame.h"
#include "GameFramework/GameUserSettings.h"
#include "Kismet/GameplayStatics.h"

void UFW1_GameInstance::Init()
{
	Super::Init();

	const FAsyncLoadGameFromSlotDelegate OnLoaded = FAsyncLoadGameFromSlotDelegate::CreateUObject(this, &UFW1_GameInstance::LoadRecordScore);
	UGameplayStatics::AsyncLoadGameFromSlot("RecordScore", 0, OnLoaded);

	GEngine->GameUserSettings->SetVSyncEnabled(true);
	GEngine->GameUserSettings->ApplySettings(true);
	GEngine->GameUserSettings->SaveSettings();
	GEngine->Exec(GetWorld(), TEXT("t.MaxFPS 60"));
}

// bool UFW1_GameInstance::GetSoundSettingByName(FName NameEvent, FSoundSetting& OutSound)
bool UFW1_GameInstance::GetSoundSettingByName(EFW1_EventsForSoundVFX EventForSound, FSoundSetting& OutSound)
{
	bool bIsFind = false;
	FSoundSetting* SoundSettingRow;

	if (SoundSettingsTable)
	{
		// FName NameEvent = UEnum::GetValueAsName(EventForSound);
		FString StrNameEvent = UEnum::GetValueAsString(EventForSound);
		
		// int32 LenNameEvent = StrNameEvent.Len();
		// FString charToFind = TEXT(":");
		// int32 firstIdx = StrNameEvent.Find(charToFind, ESearchCase::IgnoreCase, ESearchDir::Type::FromStart, 0);
		// FString colorText = StrNameEvent.Mid(firstIdx + 1, LenNameEvent);

		FString*a=new FString(),*b=new FString();
		StrNameEvent.Split(TEXT("::"),a,b);
		FName NameEvent = FName(*b); 
		
		SoundSettingRow = SoundSettingsTable->FindRow<FSoundSetting>(NameEvent, "", false);
		if (SoundSettingRow)
		{
			bIsFind = true;
			OutSound = *SoundSettingRow;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UFW1_GameInstance::GetSoundSettingByName - SoundSettingsTable -NULL"));
	}

	return bIsFind;
}

int32 UFW1_GameInstance::GetRecordScore()
{
	int32 result = 0;
	// UFW1_SaveGame* SGame = Cast<UFW1_SaveGame>(UGameplayStatics::CreateSaveGameObject(UFW1_SaveGame::StaticClass()));
	// if (SGame)
	// {
	// 	result = SGame->RecordScore;		
	// }

	UGameplayStatics::AsyncLoadGameFromSlot("RecordScore", 0,
		FAsyncLoadGameFromSlotDelegate::CreateLambda([&result](const FString& SlotName, const int32 UserIndex, USaveGame* LoadedGame)
		{
			UFW1_SaveGame* LoadSavedGame = Cast<UFW1_SaveGame>(LoadedGame);
			// if (LoadSavedGame != nullptr)
			if (LoadSavedGame)
			{
				result = LoadSavedGame->RecordScore;
			}
		}));
	
	return result;
}

void UFW1_GameInstance::SetRecordScore(int32 CurrentScore)
{
	int32 PreviousRecordScore = GetRecordScore();

	if (PreviousRecordScore < CurrentScore)
	{
		UFW1_SaveGame* SGame = Cast<UFW1_SaveGame>(UGameplayStatics::CreateSaveGameObject(UFW1_SaveGame::StaticClass()));
		if (SGame)
		{
			SGame->RecordScore = CurrentScore;
			// UGameplayStatics::SaveGameToSlot(SGame, "RecordScore", 0);
			UGameplayStatics::AsyncSaveGameToSlot(SGame, "RecordScore", 0);
		}	
	}
}

void UFW1_GameInstance::SetNewRecordScore(int32 NewRecordScore)
{
	if (NewRecordScore > Session_RecordScore)
	{
		Session_RecordScore = NewRecordScore;
		if (UGameplayStatics::DoesSaveGameExist("RecordScore", 0))
		{
			const FAsyncLoadGameFromSlotDelegate OnLoaded = FAsyncLoadGameFromSlotDelegate::CreateUObject(this, &UFW1_GameInstance::SaveRecordScore);
			UGameplayStatics::AsyncLoadGameFromSlot("RecordScore", 0, OnLoaded);
		}
		else
		{
			UFW1_SaveGame* SGame = Cast<UFW1_SaveGame>(UGameplayStatics::CreateSaveGameObject(UFW1_SaveGame::StaticClass()));
			if (SGame)
			{
				SGame->RecordScore = Session_RecordScore;
				UGameplayStatics::AsyncSaveGameToSlot(SGame, "RecordScore", 0);
			}				
		}
	}
}

void UFW1_GameInstance::LoadRecordScore(const FString& SlotName, const int32 UserIndex, USaveGame* LoadedGame)
{
	UFW1_SaveGame* LoadSaveGame = Cast<UFW1_SaveGame>(LoadedGame);
	if (LoadedGame)
	{
		Session_RecordScore = LoadSaveGame->RecordScore;
	}
}

void UFW1_GameInstance::SaveRecordScore(const FString& SlotName, const int32 UserIndex, USaveGame* LoadedGame)
{
	UFW1_SaveGame* SGame = Cast<UFW1_SaveGame>(LoadedGame);
	if (SGame)
	{
		SGame->RecordScore = Session_RecordScore;
		UGameplayStatics::AsyncSaveGameToSlot(SGame, "RecordScore", 0);
	}	
}
