// Fill out your copyright notice in the Description page of Project Settings.


#include "FW1_MenuHUD.h"

void AFW1_MenuHUD::BeginPlay()
{
	Super::BeginPlay();

	if (MenuWidgetClass)
	{
		const auto MenuWidget = CreateWidget<UUserWidget>(GetWorld(), MenuWidgetClass);
		if (MenuWidget)
		{
			MenuWidget->AddToViewport();

			// FInputModeGameOnly InputMode;	// фокус на игру
			FInputModeUIOnly InputMode;			// фокус на UI
			InputMode.SetWidgetToFocus(MenuWidget->TakeWidget());
			InputMode.SetLockMouseToViewportBehavior(EMouseLockMode::LockOnCapture);
			GetOwningPlayerController()->SetInputMode(InputMode);
			GetOwningPlayerController()->SetShowMouseCursor(true);
		}
	}
}
