// Fill out your copyright notice in the Description page of Project Settings.


#include "FW1_MenuModeBase.h"
#include "FW1_MenuHUD.h"
#include "Menu/FW1_MenuPlayerController.h"

AFW1_MenuModeBase::AFW1_MenuModeBase()
{
	PlayerControllerClass = AFW1_MenuPlayerController::StaticClass();
	HUDClass = AFW1_MenuHUD::StaticClass();
}
