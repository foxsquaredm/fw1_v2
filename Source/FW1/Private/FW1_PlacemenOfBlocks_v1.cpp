// Fill out your copyright notice in the Description page of Project Settings.


#include "FW1_PlacemenOfBlocks_v1.h"
#include "FW1_Block.h"
#include "FW1_GameInstance.h"
#include "FW1/FW1GameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

DEFINE_LOG_CATEGORY_STATIC(Log_POB2, All, All);

// Sets default values
AFW1_PlacemenOfBlocks_v1::AFW1_PlacemenOfBlocks_v1()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void AFW1_PlacemenOfBlocks_v1::BeginPlay()
{
	Super::BeginPlay();

	// ArrangeBlocks_V1();
	ArrangeBlocks_V2();

}

void AFW1_PlacemenOfBlocks_v1::ArrangeBlocks_V1()
{
	int32 LocationX = 0;
	int32 LocationY = 0;
	// TotalBlocksInGame = AmountX * AmountY;
	for (int32 curX = 0; curX < AmountX; ++curX)
	{
		for (int32 curY = 0; curY < AmountY; ++curY)
		{
			LocationX = StartAxisX + (curX * Padding);
			LocationY = StartAxisY + (curY * Padding);
	
			FVector curLocation = FVector(LocationX, LocationY, 250);
			FTransform curTransform = UKismetMathLibrary::MakeTransform(curLocation,FRotator::ZeroRotator, FVector(1, 1, 1));

			int32 NumberBlockTypes = AllBlocks.Num(); 
			int32 curIndexType = UKismetMathLibrary::RandomIntegerInRange(0,NumberBlockTypes - 1);

			// !!! NEW BLOCK-CODE
			//SetBlockRange(curIndexType);
			// int32 curIndexType = UKismetMathLibrary::RandomIntegerInRange(1,BlockRange);
			
			// AFW1_Block* curBlock = nullptr;
			if (NumberBlockTypes > 0)
			{
				auto BlocksType = AllBlocks[curIndexType];
				// auto BlocksType = AllBlocks[curIndexType - 1];
				auto CurrentBlock = Cast<AFW1_Block>(GetWorld()->SpawnActor(BlocksType, &curTransform));
				CurrentBlock->OnDeadBlock.AddDynamic(this, &AFW1_PlacemenOfBlocks_v1::OnDeadOneBlock);
				if (CurrentBlock->AmountDamage > -1)
				{
					TotalBlocksInGame++;
				}
			}
			else
			{
				// curBlock = Cast<AFW1_Block>(GetWorld()->SpawnActor(DefaultBlock, &curTransform));
				// curBlock->ChangeColor();
				Cast<AFW1_Block>(GetWorld()->SpawnActor(DefaultBlock, &curTransform));
			}
		}
	}
}

void AFW1_PlacemenOfBlocks_v1::ArrangeBlocks_V2()
{
//1. we collect an array for uniform distribution of blocks according to the probability table
//2. the set of all blocks in an array is determined by the minimum unit
	// 5	15	40	40
	// 1	3	8	8
//3. in this example an array of 20 elements distributed by probability (first line)

	TArray<FArrangeBlocks> BlockPack;

	UFW1_GameInstance* myGameInst = Cast<UFW1_GameInstance>(GetWorld()->GetGameInstance());
	if (!myGameInst) return;

	UDataTable* tablArrangeBlock = myGameInst->ArrangeBlockProbability;
	int32 MinProb = FindMinProb(tablArrangeBlock);
	int32 NumberOfBlocksInPackage = 0;
		
	FArrangeBlocks ArrangeBlock;
	// for (auto curElement : myGameInst->ArrangeBlockProbability->GetAllRows())
	// {
	// 	FProbabilityBlock* currentRow = (FProbabilityBlock*)curElement.Value;
	// 	ArrangeBlock.TypeBlock				= currentRow->TypeBlock;
	// 	ArrangeBlock.ProbabilityBlock		= currentRow->ProbabilityBlock;
	// 	int32 QuantityBlock = currentRow->ProbabilityBlock / MinProb;
	// 	ArrangeBlock.AmountBlocks			= QuantityBlock;
	// 	ArrangeBlock.CurrentBlockBalance	= QuantityBlock;
	// 	NumberOfBlocksInPackage = NumberOfBlocksInPackage + QuantityBlock;
	// 	BlockPack.Add(ArrangeBlock);
	// }

	TArray<FName> RowNames = tablArrangeBlock->GetRowNames();
	NumberOfBlocksInPackage = RowNames.Num();
	int32 totalBalance = 0;
	int32 currentBalance = 0;
	
	for (auto RowName : RowNames)
	{
		FProbabilityBlock* RowData = tablArrangeBlock->FindRow<FProbabilityBlock>(RowName, "");
		if (RowData)
		{
			ArrangeBlock.TypeBlock			= RowData->TypeBlock;
			ArrangeBlock.ProbabilityBlock	= RowData->ProbabilityBlock;
			int32 QuantityBlock				= RowData->ProbabilityBlock / MinProb;
			ArrangeBlock.AmountBlocks		= QuantityBlock;
			ArrangeBlock.CurrentBlockBalance = QuantityBlock;
			// NumberOfBlocksInPackage			= NumberOfBlocksInPackage + QuantityBlock;
			totalBalance = totalBalance + QuantityBlock;
			BlockPack.Add(ArrangeBlock);

			UE_LOG(Log_POB2, Display, TEXT("BlockPack, 1x: %p, 2x: %i, 3x: %i, 4x: %i"), *RowData->TypeBlock, RowData->ProbabilityBlock, QuantityBlock, totalBalance);
		}
	}

	for (int8 i = BlockPack.Num() - 1; i >= 0; i--)
	{
		// ещё одна проверка данных в массиве
		UE_LOG(Log_POB2, Display, TEXT("MyDebug: BlockPack, TypeBlock: %s, ProbabilityBlock: %d, AmountBlocks: %d, CurrentBlockBalance: %d"),
			*BlockPack[i].TypeBlock->GetName(),
			BlockPack[i].ProbabilityBlock,
			BlockPack[i].AmountBlocks,
			BlockPack[i].CurrentBlockBalance);
	}
	
	// int32 BalanceOfBlocksInPackage = NumberOfBlocksInPackage; 
	int32 LocationX = 0;
	int32 LocationY = 0;
	// TotalBlocksInGame = AmountX * AmountY;
	for (int32 curX = 0; curX < AmountX; ++curX)
	{
		for (int32 curY = 0; curY < AmountY; ++curY)
		{
			// new pack blocks
			if (currentBalance == 0)
			{
				// BalanceOfBlocksInPackage = NumberOfBlocksInPackage;
				currentBalance = totalBalance;
				for (auto curElement : BlockPack)
				{
					curElement.CurrentBlockBalance	= curElement.AmountBlocks;
				}
			}
	
			// new coordinat block
			LocationX = StartAxisX + (curX * Padding);
			LocationY = StartAxisY + (curY * Padding);
			FVector curLocation = FVector(LocationX, LocationY, 250);
			FTransform curTransform = UKismetMathLibrary::MakeTransform(curLocation,FRotator::ZeroRotator, FVector(1, 1, 1));
	
			// new random from pack blocks
			int32 curIndexBlock = UKismetMathLibrary::RandomIntegerInRange(1,NumberOfBlocksInPackage);
			FArrangeBlocks currentLine = BlockPack[curIndexBlock - 1];

			// GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10.f, FColor::Green, *FString::Printf(TEXT("curIndexBlock: %d"), curIndexBlock));
			UE_LOG(Log_POB2, Display, TEXT("curIndexBlock_f: %d"), curIndexBlock);
			// GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10.f, FColor::Green, *FString::Printf(TEXT("CurrentLine: %p"), currentLine));
			// GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10.f, FColor::Green, *FString::Printf(TEXT("CurrentBlockBalance: %i"), currentLine->CurrentBlockBalance));
			UE_LOG(Log_POB2, Display, TEXT("CurrentBlockBalance_f: %i"), currentLine.CurrentBlockBalance);
			
			while (currentLine.CurrentBlockBalance <= 0)
			// int32 CBC = 0;
			// while (CBC != 5)
				{
					// CBC ++;
					curIndexBlock = UKismetMathLibrary::RandomIntegerInRange(1,NumberOfBlocksInPackage);
					currentLine = BlockPack[curIndexBlock - 1];

					// GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10.f, FColor::Green, *FString::Printf(TEXT("curIndexBlock: %d"), curIndexBlock));
					UE_LOG(Log_POB2, Display, TEXT("curIndexBlock: %d"), curIndexBlock);
					// GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10.f, FColor::Green, *FString::Printf(TEXT("CurrentLine: %p"), currentLine));
					// GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10.f, FColor::Green, *FString::Printf(TEXT("CurrentBlockBalance: %i"), currentLine->CurrentBlockBalance));
					UE_LOG(Log_POB2, Display, TEXT("CurrentBlockBalance: %i"), currentLine.CurrentBlockBalance);
				}
			// currentLine->CurrentBlockBalance = currentLine->CurrentBlockBalance - 1;
			currentLine.CurrentBlockBalance --;
	
			// create block from pack blocks
			auto CurrentBlock = Cast<AFW1_Block>(GetWorld()->SpawnActor(currentLine.TypeBlock, &curTransform));
			CurrentBlock->OnDeadBlock.AddDynamic(this, &AFW1_PlacemenOfBlocks_v1::OnDeadOneBlock);
			if (CurrentBlock->AmountDamage > -1)
			{
				TotalBlocksInGame++;
				currentBalance--;
			}
		}
	}
	
}

int32 AFW1_PlacemenOfBlocks_v1::FindMinProb(UDataTable* ArrangeBlockProbability)
{
	int32 MinProb = 0;

	TArray<FName> RowNames = ArrangeBlockProbability->GetRowNames();
	for (auto RowName : RowNames)
	{
		FProbabilityBlock* RowData = ArrangeBlockProbability->FindRow<FProbabilityBlock>(RowName, "");
		if (RowData)
		{
			if (MinProb == 0)
			{
				MinProb = RowData->ProbabilityBlock;
			}
			else
			{
				MinProb = FMath::Min(MinProb, RowData->ProbabilityBlock);	
			}
		}
	}
	
	return MinProb;	
}

void AFW1_PlacemenOfBlocks_v1::OnDeadOneBlock()
{
	TotalBlocksInGame--;

	FString TextInfoScore = FString::Printf(TEXT("TotalBlocksInGame: %s "), *FString::FromInt(TotalBlocksInGame));
	GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10.f, FColor::Orange, *TextInfoScore);
	
	if (TotalBlocksInGame == 0)
	{
		FString Stat = FString::Printf(TEXT("You Won!!!"));
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, Stat, true, FVector2D(1.5f, 1.5f));

		const auto GameMode = Cast<AFW1GameModeBase>(GetWorld()->GetAuthGameMode());
		if(!GameMode) return;

		GameMode->OnGameOver();
		
	// 	const FName LevelName = "NONE";
	// 	UGameplayStatics::OpenLevel(this, LevelName);
	}
}
