// Fill out your copyright notice in the Description page of Project Settings.


#include "FW1_PlayerController.h"
#include "FW1/FW1GameModeBase.h"
#include "GameFramework/GameModeBase.h"


void AFW1_PlayerController::BeginPlay()
{
	Super::BeginPlay();

	if(GetWorld())
	{
		const auto GameMode = Cast<AFW1GameModeBase>(GetWorld()->GetAuthGameMode());
		if (GameMode)
		{
			GameMode->OnChangedStatusGame.AddUObject(this, &AFW1_PlayerController::OnChangedStatusGame);
		}
	}
}

void AFW1_PlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	if (!InputComponent) return;
	
	InputComponent->BindAction("PauseGame", IE_Pressed, this, &AFW1_PlayerController::OnPauseGame);
}

void AFW1_PlayerController::OnPauseGame()
{
	if(!GetWorld() || !GetWorld()->GetAuthGameMode()) return;
	// GetWorld()->GetAuthGameMode()->SetPause(this);
	const auto GameMode = Cast<AFW1GameModeBase>(GetWorld()->GetAuthGameMode());
	if(!GameMode) return;

	GameMode->OnPauseGame();
}

void AFW1_PlayerController::OnChangedStatusGame(EFW1_StatusGame StatusGame)
{
	if (StatusGame == EFW1_StatusGame::inPlayGame)
	{
		SetInputMode(FInputModeGameOnly());
		bShowMouseCursor = false;
	}
	else
	{
		SetInputMode(FInputModeUIOnly());
		bShowMouseCursor = true;
	}
}
