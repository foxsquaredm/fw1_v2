// Fill out your copyright notice in the Description page of Project Settings.


#include "FW1_PlayerState.h"
#include "FW1/FW1GameModeBase.h"

void AFW1_PlayerState::SubtractionLive()
{
	--LivesNum;
	OnChangedLives.Broadcast();
	if (LivesNum < 0)
	{
		if(!GetWorld()) return;
		const auto GameMode = Cast<AFW1GameModeBase>(GetWorld()->GetAuthGameMode());
		if(!GameMode) return;

		GameMode->OnGameOver();
	}
}

void AFW1_PlayerState::AddScore(int32 cNum)
{
	ScoresNum = ScoresNum + cNum;
	OnChangedScore.Broadcast();
}
