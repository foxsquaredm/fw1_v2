// Fill out your copyright notice in the Description page of Project Settings.


#include "Menu/FW1_BackToMainMenuWidget.h"
#include "FW1_GameInstance.h"
#include "Kismet/GameplayStatics.h"

void UFW1_BackToMainMenuWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (Button_BackToMainMenu)
	{
		Button_BackToMainMenu->OnClicked.AddDynamic(this, &UFW1_BackToMainMenuWidget::OnBackToMainMenu);
	}
}

void UFW1_BackToMainMenuWidget::OnBackToMainMenu()
{
	if (!GetWorld()) return;
	
	const auto STUGameInstance = GetWorld()->GetGameInstance<UFW1_GameInstance>();
	if (!STUGameInstance) return;

	if (STUGameInstance->GetMenuLevelName().IsNone())
	{
		return;
	}

	UGameplayStatics::OpenLevel(this, STUGameInstance->GetMenuLevelName());
}
