// Fill out your copyright notice in the Description page of Project Settings.


#include "Menu/FW1_GameOverWidget.h"
#include "FW1_PlayerState.h"
#include "Components/TextBlock.h"
#include "FW1/FW1GameModeBase.h"
#include "Kismet/GameplayStatics.h"

void UFW1_GameOverWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if(GetWorld())
	{
		const auto GameMode = Cast<AFW1GameModeBase>(GetWorld()->GetAuthGameMode());
		if (GameMode)
		{
			GameMode->OnChangedStatusGame.AddUObject(this, &UFW1_GameOverWidget::ShowResultGame);
		}
	}
		
	if (Button_Restart)
	{
		Button_Restart->OnClicked.AddDynamic(this, &UFW1_GameOverWidget::OnPressRestart);
	}
}

void UFW1_GameOverWidget::ShowResultGame(EFW1_StatusGame StatusGame)
{
	if (StatusGame != EFW1_StatusGame::inGameOver) return;
		
	const auto myPlayerState = Cast<AFW1_PlayerState>(GetOwningPlayer()->PlayerState);
	int32 lives = myPlayerState->GetLivesNum();

	FString Str = TEXT("");
	if (lives < 0)
	{
		Str = TEXT("You lose!");  
	}
	else
	{
		Str = TEXT("You won!");
	}
	const FText& TextResult = FText::FromString(Str);
	if(!TextBlock_ResultGame) return;
	TextBlock_ResultGame->SetText(TextResult);
}

void UFW1_GameOverWidget::OnPressRestart()
{
	const FString CurrentLEvelName = UGameplayStatics::GetCurrentLevelName(this);
	UGameplayStatics::OpenLevel(this, FName(CurrentLEvelName));
}
