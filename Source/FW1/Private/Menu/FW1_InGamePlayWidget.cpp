// Fill out your copyright notice in the Description page of Project Settings.


#include "Menu/FW1_InGamePlayWidget.h"

#include "FW1_GameInstance.h"
#include "FW1_PlayerState.h"
// #include "Components/Image.h"
// #include "Components/TextBlock.h"

void UFW1_InGamePlayWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();
	
	const auto myPlayerState = Cast<AFW1_PlayerState>(GetOwningPlayer()->PlayerState);
	if (!myPlayerState) return;

	SetVisibilityLives();
	SetTextScore();
	SetTextRecordScore();
	
	myPlayerState->OnChangedLives.AddDynamic(this, &UFW1_InGamePlayWidget::SetVisibilityLives);
	myPlayerState->OnChangedScore.AddDynamic(this, &UFW1_InGamePlayWidget::SetTextScore);
	
}

void UFW1_InGamePlayWidget::SetVisibilityLives()
{
	const auto myPlayerState = Cast<AFW1_PlayerState>(GetOwningPlayer()->PlayerState);
	int32 lives = myPlayerState->GetLivesNum();

	switch (lives)
	{
	case (3):
		{
			SetImageStar(ImageStar_1, true);
			SetImageStar(ImageStar_2, true);
			SetImageStar(ImageStar_3, true);
	
			break;
		}
	case (2):
		{
			SetImageStar(ImageStar_1, true);
			SetImageStar(ImageStar_2, true);
			SetImageStar(ImageStar_3, false);
	
			break;
		}
	case (1):
		{
			SetImageStar(ImageStar_1, true);
			SetImageStar(ImageStar_2, false);
			SetImageStar(ImageStar_3, false);
	
			break;
		}
	case (0):
		{
			SetImageStar(ImageStar_1, false);
			SetImageStar(ImageStar_2, false);
			SetImageStar(ImageStar_3, false);
	
			break;
		}
		default:
			break;
	}
}

void UFW1_InGamePlayWidget::SetImageStar(USizeBox* ImageStar_Num, bool Visible)
{
	if(!ImageStar_Num) return;
	ImageStar_Num->SetVisibility(Visible ? ESlateVisibility::Visible : ESlateVisibility::Hidden);
}

void UFW1_InGamePlayWidget::SetTextScore()
{
	const auto myPlayerState = Cast<AFW1_PlayerState>(GetOwningPlayer()->PlayerState);
	//int32 CurrentScore = myPlayerState->GetScoresNum();
	FString CurrentScore = FString::FromInt(myPlayerState->GetScoresNum());
	
	const FText& TextResult = FText::FromString(CurrentScore);
	if(!TextValue_Score) return;
	TextValue_Score->SetText(TextResult);
}

void UFW1_InGamePlayWidget::SetTextRecordScore()
{
	const auto FW1_GameInstance = GetWorld()->GetGameInstance<UFW1_GameInstance>();
	if (!FW1_GameInstance) return;
	
	// FString CurrentScore = FString::FromInt(FW1_GameInstance->GetRecordScore());
	FString CurrentScore = FString::FromInt(FW1_GameInstance->Session_RecordScore);
	const FText& TextResult = FText::FromString(CurrentScore);
	if(!TextValue_RecordScore) return;
	TextValue_RecordScore->SetText(TextResult);
}
