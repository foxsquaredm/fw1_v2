// Fill out your copyright notice in the Description page of Project Settings.


#include "Menu/FW1_MainMenuWidget.h"
#include "FW1_GameInstance.h"
#include "Kismet/GameplayStatics.h"

void UFW1_MainMenuWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (Button_Start)
	{
		Button_Start->OnClicked.AddDynamic(this, &UFW1_MainMenuWidget::OnPressStart);
	}
	if (Button_Records)
	{
		Button_Records->OnClicked.AddDynamic(this, &UFW1_MainMenuWidget::OnPressRecords);
	}
	if (Button_Quit)
	{
		Button_Quit->OnClicked.AddDynamic(this, &UFW1_MainMenuWidget::OnPressQuit);
	}
}

void UFW1_MainMenuWidget::OnPressStart()
{
	if (!GetWorld()) return;

	const auto FW1_GameInstance = GetWorld()->GetGameInstance<UFW1_GameInstance>();
	if (!FW1_GameInstance) return;

	UGameplayStatics::OpenLevel(this, FW1_GameInstance->GetGameLevelName());
}

void UFW1_MainMenuWidget::OnPressRecords()
{
	GEngine->AddOnScreenDebugMessage(INDEX_NONE, 10.f, FColor::Red, *FString::Printf(TEXT("not work - in developing!")));
}

void UFW1_MainMenuWidget::OnPressQuit()
{
	UKismetSystemLibrary::QuitGame(this, GetOwningPlayer(), EQuitPreference::Quit, true);
}
