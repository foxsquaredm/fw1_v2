// Fill out your copyright notice in the Description page of Project Settings.


#include "Menu/FW1_PauseWidget.h"
#include "FW1/FW1GameModeBase.h"
#include "GameFramework/GameModeBase.h"
#include "Kismet/GameplayStatics.h"

void UFW1_PauseWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (Button_Continue)
	{
		Button_Continue->OnClicked.AddDynamic(this, &UFW1_PauseWidget::OnPressContinue);
	}
	if (Button_Restart)
	{
		Button_Restart->OnClicked.AddDynamic(this, &UFW1_PauseWidget::OnPressRestart);
	}
}

void UFW1_PauseWidget::OnPressContinue()
{
	if(!GetWorld() || !GetWorld()->GetAuthGameMode()) return;

	GetWorld()->GetAuthGameMode()->ClearPause();
}

void UFW1_PauseWidget::OnPressRestart()
{
	const FString CurrentLEvelName = UGameplayStatics::GetCurrentLevelName(this);
	UGameplayStatics::OpenLevel(this, FName(CurrentLEvelName));
}
