// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FW1/FW1_CoreTypes.h"
#include "Components/ActorComponent.h"
#include "FW1_SoundComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FW1_API UFW1_SoundComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFW1_SoundComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
// 	// Called every frame
// 	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION()
	void PlayGameSoundEvent(EFW1_EventsForSoundVFX SoundVFX);

	
};
