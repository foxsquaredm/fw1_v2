// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FW1/FW1_CoreTypes.h"
#include "Components/FW1_SoundComponent.h"
#include "GameFramework/Actor.h"
#include "FW1_Ball.generated.h"

// DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeadBall);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDeadBall, bool, isBonus);

UCLASS()
class FW1_API AFW1_Ball : public AActor
{
	GENERATED_BODY()
	
	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* StaticMeshComponent;
	
public:	
	// Sets default values for this actor's properties
	AFW1_Ball();

	UPROPERTY(BlueprintAssignable)
	FOnDeadBall OnDeadBall;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Перезаписываем функцию для вызова после инициализации.
	virtual void PostInitProperties() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Components")
	UFW1_SoundComponent* SoundComponent;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, 
		AActor* OtherActor, UPrimitiveComponent* OtherComp, 
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	UPROPERTY(EditAnywhere)
	FVector BallDirection = FVector(10,10,0);
	
	UPROPERTY(EditAnywhere)
	float BallSpeed = 0.f;

	UPROPERTY(EditAnywhere)
	bool isBonus = false;
	
	UFUNCTION(BlueprintCallable)
	void ChangeSpeed(float Coefficient);

	// UFUNCTION()
	// void PlaySoundForGame(EFW1_EventsForSoundVFX SoundVFX);
	
	UFUNCTION(BlueprintCallable)
	void ChangeColor();

	float GetDefaultSpeedBall();
};




