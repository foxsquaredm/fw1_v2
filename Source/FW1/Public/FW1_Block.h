// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FW1/FW1_CoreTypes.h"
#include "Components/FW1_SoundComponent.h"
#include "GameFramework/Actor.h"
#include "FW1_Block.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeadBlock);

UCLASS()
class FW1_API AFW1_Block : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFW1_Block();

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* MeshBox;

	UPROPERTY(BlueprintAssignable)
	FOnDeadBlock OnDeadBlock;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Components")
	UFW1_SoundComponent* SoundComponent;
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent,
	                    AActor* OtherActor, UPrimitiveComponent* OtherComp,
	                    int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp,
	           AActor* OtherActor, UPrimitiveComponent* OtherComp,
	           FVector NormalImpulse, const FHitResult& Hit);
	

	UPROPERTY()
	bool InMoving;

	//if we don't take block damage into account, then set the value to -1,
	//if we want to destroy the block, then set the value to 0
	//if we want to destroy after N damage, then set the value to greater than 0 
	UPROPERTY(EditAnywhere)
	int32 AmountDamage = -1;

	//similar to the previous variable
	UPROPERTY(EditAnywhere)
	int32 AmountDamageBeforMoving = -1;

	UPROPERTY(EditAnywhere)
	EFW1_BonusTypes BonusType = EFW1_BonusTypes::WithoutBonus; 

	UPROPERTY(EditAnywhere)
	int32 ScoreInBlock = 10; 

	// UFUNCTION()
	// void PlaySoundForGame(EFW1_EventsForSoundVFX EventForSound);
	
	UFUNCTION()
	void ChangeColor();

	UFUNCTION()
	void OnDeadCurrentBlock(EFW1_EventsForSoundVFX SoundVFX = EFW1_EventsForSoundVFX::NONE);
	
};
