// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FW1_Ball.h"
#include "FW1/FW1_CoreTypes.h"
#include "Components/BoxComponent.h"
#include "Components/FW1_SoundComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "GameFramework/Pawn.h"
// #include "Sound/SoundCue.h"
#include "FW1_Carriage.generated.h"

// class UFW1_SoundComponent;

UCLASS()
class FW1_API AFW1_Carriage : public APawn
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* MeshCarriega;
	
	UPROPERTY(VisibleAnywhere)
	UBoxComponent* BoxCollision;
	
	UPROPERTY(VisibleAnywhere)
	USphereComponent* SphereCollision;

	UPROPERTY()
	UFloatingPawnMovement* MovementComponent;
	
public:
	// Sets default values for this pawn's properties
	AFW1_Carriage();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
	// 	USoundCue* StartGameSound;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Components")
	UFW1_SoundComponent* SoundComponent;

public:	
	// // Called every frame
	// virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, 
		AActor* OtherActor, UPrimitiveComponent* OtherComp, 
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	UPROPERTY()
	AFW1_Ball* PlayingBall;

	// TSubclassOf<YourClass> BlueprintVar;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ClassBall")
    TSubclassOf<class AFW1_Ball> DefaultBall = nullptr;
	
private:
	
	void MoveRight(float Amount);

	// UFUNCTION()
	// void PlaySoundForGame(EFW1_EventsForSoundVFX EventForSound);

	UFUNCTION()
	void SpawnBall(bool isBonus);
	UFUNCTION()
	void OnDeadPlayingBall(bool isBonus);
	// double AmountChange = 5;
	UFUNCTION()
	void ChangeLongCarriage(float Coefficient);
	UFUNCTION()
	FVector GetDefaultScaleCarriage();
	UFUNCTION()
	void ResetAllSettings();
};
