// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "FW1/FW1_CoreTypes.h"
#include "GameFramework/HUD.h"
#include "FW1_GameHUD.generated.h"

UCLASS()
class FW1_API AFW1_GameHUD : public AHUD
{
	GENERATED_BODY()

public:
	virtual void DrawHUD() override;
	
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="UI")
	TSubclassOf<UUserWidget> GameWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="UI")
	TSubclassOf<UUserWidget> PauseWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="UI")
	TSubclassOf<UUserWidget> GameOverWidgetClass;
    
	virtual void BeginPlay() override;
    
private:
	UPROPERTY()
	TMap<EFW1_StatusGame, UUserWidget*> GameWidgets;
	
	UPROPERTY()
	UUserWidget* CurrentWidget = nullptr; 
        
	void OnChangedStatusGame(EFW1_StatusGame StatusGame);
};
