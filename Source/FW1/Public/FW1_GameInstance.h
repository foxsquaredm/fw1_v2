// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "FW1/FW1_CoreTypes.h"
#include "FW1_GameInstance.generated.h"

UCLASS()
class FW1_API UFW1_GameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	void Init() override;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameSetting")
	UDataTable* SoundSettingsTable = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameSetting")
	UDataTable* ArrangeBlockProbability = nullptr;
	
	bool GetSoundSettingByName(EFW1_EventsForSoundVFX EventForSound, FSoundSetting& OutSound);

	FName GetMenuLevelName() const { return MenuLevelName; }
	FName GetGameLevelName() const { return GameLevelName; }

	UFUNCTION()
	int32 GetRecordScore();
	UFUNCTION()
	void SetRecordScore(int32 CurrentScore);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category ="SaveGame")
	int32 Session_RecordScore = 0;

	UFUNCTION(BlueprintCallable, Category="SaveGame")
	void SetNewRecordScore(int32 NewRecordScore);
	
private:
	UPROPERTY(EditDefaultsOnly, Category = "Game")
	FName MenuLevelName = NAME_None;
	UPROPERTY(EditDefaultsOnly, Category = "Game")
	FName GameLevelName = NAME_None;

	UFUNCTION(Category="SaveGame")
	void LoadRecordScore(const FString &SlotName, const int32 UserIndex, USaveGame* LoadedGame);

	UFUNCTION(Category="SaveGame")
	void SaveRecordScore(const FString &SlotName, const int32 UserIndex, USaveGame* LoadedGame);

};
