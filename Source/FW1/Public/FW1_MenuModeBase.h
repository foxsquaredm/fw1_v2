// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FW1_MenuModeBase.generated.h"


UCLASS()
class FW1_API AFW1_MenuModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFW1_MenuModeBase();
};
