// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FW1_Block.h"
#include "FW1/FW1_CoreTypes.h"
#include "GameFramework/Actor.h"
#include "FW1_PlacemenOfBlocks_v1.generated.h"

USTRUCT(BlueprintType)
struct FArrangeBlocks
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<class AFW1_Block> TypeBlock = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 ProbabilityBlock = 0;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 AmountBlocks = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 CurrentBlockBalance = 0;
		
};

UCLASS()
class FW1_API AFW1_PlacemenOfBlocks_v1 : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFW1_PlacemenOfBlocks_v1();

	UPROPERTY()
	int32 AmountX = 15; 
	UPROPERTY()
	int32 AmountY = 31;

	UPROPERTY()
	int32 StartAxisX = -450;
	UPROPERTY()
	int32 StartAxisY = -1800;

	UPROPERTY()
	int32 Padding = 120;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ClassBlocks")
	TSubclassOf<class AFW1_Block> DefaultBlock = nullptr;
	// TSubclassOf<AFW1_Block> DefaultBlock;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ClassBlocks")
	//TArray<AFW1_Block*> AllBlocks;
	TArray<TSubclassOf<AFW1_Block>> AllBlocks;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void ArrangeBlocks_V1();
	
	/////////////////////////////////////////////////////////////////
	// to proportionally calculate the probability of obtaining blocks
	UFUNCTION()
	void ArrangeBlocks_V2();

	UFUNCTION()
	int32 FindMinProb(UDataTable* ArrangeBlockProbability);
	
	
// 	UPROPERTY()
// 	int32 TotalBlocks = 0;
//
// 	TArray<FProbabilityBlock> CurrentProbabilityBlock;
//
// 	UPROPERTY()
// 	float ProbabilityForOneBlock = 0.0f;
// 	
// 	UPROPERTY()
// 	float PercentMinBlock = 0.0f;
//
// 	// blocks range for probability selection
// 	UPROPERTY()
// 	int32 BlockRange = 0;
//
// 	UFUNCTION()
// 	int32 SetTotalAllBlocks();
//
// 	UFUNCTION()
// 	void SetPercentMinBlock();
//
// 	UFUNCTION()
// 	float SetProbabilityForOneBlock();
// 	
// 	UFUNCTION()
// 	void SetCurProbabilityBlocks(float PrevProbabilityNumber);
//
// 	UFUNCTION()
// 	void SetBlockRange(float PrevProbabilityNumber);
	
public:	
	// // Called every frame
	// virtual void Tick(float DeltaTime) override;

	UPROPERTY()
	int32 TotalBlocksInGame = 0;

	UFUNCTION()
	void OnDeadOneBlock();
		
};
