// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FW1/FW1_CoreTypes.h"
#include "GameFramework/PlayerController.h"
#include "FW1_PlayerController.generated.h"

UCLASS()
class FW1_API AFW1_PlayerController : public APlayerController
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
	virtual void SetupInputComponent() override;

private:
	void OnPauseGame();
	void OnChangedStatusGame(EFW1_StatusGame StatusGame);
};
