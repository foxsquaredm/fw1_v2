// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "FW1_PlayerState.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnChangedLives);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnChangedScore);

UCLASS()
class FW1_API AFW1_PlayerState : public APlayerState
{
	GENERATED_BODY()

public:

	// UPROPERTY(BlueprintAssignable)
	FOnChangedLives OnChangedLives;
	// UPROPERTY(BlueprintAssignable)
	FOnChangedScore OnChangedScore;
	
	UFUNCTION(BlueprintCallable)
	void SubtractionLive();
	UFUNCTION(BlueprintCallable)
	int32 GetLivesNum() const { return LivesNum;}

	UFUNCTION(BlueprintCallable)
	void AddScore(int32 cNum); 
	UFUNCTION(BlueprintCallable)
	int32 GetScoresNum() const { return ScoresNum;}
	
private:
	
	UPROPERTY()
	int32 LivesNum = 3;

	UPROPERTY()
	int32 ScoresNum = 0;
	
};
