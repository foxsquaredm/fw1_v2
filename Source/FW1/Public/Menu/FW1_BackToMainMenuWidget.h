// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "FW1_BackToMainMenuWidget.generated.h"

UCLASS()
class FW1_API UFW1_BackToMainMenuWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(meta = (BindWidget))
	UButton* Button_BackToMainMenu;

	virtual void NativeOnInitialized() override;

private:
	UFUNCTION()
	void OnBackToMainMenu();
};
