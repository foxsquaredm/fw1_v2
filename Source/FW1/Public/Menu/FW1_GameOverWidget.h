// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FW1/FW1_CoreTypes.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "FW1_GameOverWidget.generated.h"

UCLASS()
class FW1_API UFW1_GameOverWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(meta=(BindWidget))
	UButton* Button_Restart;

	UPROPERTY(meta=(BindWidget))
	UTextBlock* TextBlock_ResultGame;
	
	virtual void NativeOnInitialized() override;

private:
	UFUNCTION()
	void ShowResultGame(EFW1_StatusGame StatusGame);

	UFUNCTION()
	void OnPressRestart();
};
