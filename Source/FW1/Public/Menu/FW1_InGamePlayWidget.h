// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Image.h"
#include "Components/SizeBox.h"
#include "Components/TextBlock.h"
#include "FW1_InGamePlayWidget.generated.h"

UCLASS()
class FW1_API UFW1_InGamePlayWidget : public UUserWidget
{
	GENERATED_BODY()

// public:
// 	void SetVisibilityLives();
// 	void SetImageStar(UImage* ImageStar_Num, bool Visible);
// 	void SetTextScore();
	
protected:
	UPROPERTY(meta=(BindWidget))
	// UImage* ImageStar_1;
	USizeBox* ImageStar_1;

	UPROPERTY(meta=(BindWidget))
	// UImage* ImageStar_2;
	USizeBox* ImageStar_2;

	UPROPERTY(meta=(BindWidget))
	// UImage* ImageStar_3;
	USizeBox* ImageStar_3;
	
	UPROPERTY(meta=(BindWidget))
	UTextBlock* TextValue_Score;

	UPROPERTY(meta=(BindWidget))
	UTextBlock* TextValue_RecordScore;

	virtual void NativeOnInitialized() override;

private:
	UFUNCTION()
	void SetVisibilityLives();
	UFUNCTION()
	// void SetImageStar(UImage* ImageStar_Num, bool Visible);
	void SetImageStar(USizeBox* ImageStar_Num, bool Visible);

	UFUNCTION()
	void SetTextScore();

	UFUNCTION()
	void SetTextRecordScore();
};
