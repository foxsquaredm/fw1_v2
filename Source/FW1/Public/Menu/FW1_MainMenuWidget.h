// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "FW1_MainMenuWidget.generated.h"


UCLASS()
class FW1_API UFW1_MainMenuWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(meta=(BindWidget))
	UButton* Button_Start;

	UPROPERTY(meta=(BindWidget))
	UButton* Button_Records;

	UPROPERTY(meta=(BindWidget))
	UButton* Button_Quit;

	virtual void NativeOnInitialized() override;
	
private:
	UFUNCTION()
	void OnPressStart();

	UFUNCTION()
	void OnPressRecords();
	
	UFUNCTION()
	void OnPressQuit();
};
